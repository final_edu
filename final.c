#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Structure declaration
 */
typedef struct _jugador jugador;
struct 
_jugador
{
  char nombre[128];
  int anotaciones;
};

typedef struct _futbol futbol;
struct
_futbol
{
  /*
   * Here goes the distinct fields
   */
};

typedef struct _basket basket;
struct
_basket
{
  /*
   * Here goes the distinct fields
   */
  int triples;
};

typedef struct _deporte deporte;
struct
_deporte
{
  char *nombre;
  int victorias;
  int derrotas;
  int anotaciones_favor;
  int anotaciones_contra;
  jugador estrella;

  /*  
   * 0 = futbol
   * 1 = basket
   */
  int num_deporte;
  void *tipo_deporte;
};

int
get_equipo(deporte *equipo)
{
  char tmp[1024];

/* 
 * Prints user message
 */
  switch ( equipo->num_deporte)
  {

    case 0: 
      printf("[Nombre equipo],[victorias],[derrotas],[goles a favor]"
          ",[goles en contra] : ");
      break;

    case 1:
      printf("[Nombre equipo],[victorias],[derrotas],[anotaciones a favor]"
          ",[anotaciones en contra] : ");
      break;

  }

/*
 * Gets data
 */
  scanf("%s", tmp);
  equipo->nombre = strtok(tmp, ",");
  sscanf(strtok(NULL, ","), "%d",  &equipo->victorias);
  sscanf(strtok(NULL, ","), "%d",  &equipo->derrotas);
  sscanf(strtok(NULL, ","), "%d",  &equipo->anotaciones_favor);
  sscanf(strtok(NULL, ","), "%d",  &equipo->anotaciones_contra);


/*
 * Gets specific data
 */
  switch ( equipo->num_deporte)
  {

    case 0: 
      break;

    case 1:
      break;

  }

  printf("[Nombre jugador estrella] : ");
  scanf("%s"
      , equipo->estrella.nombre
      );

  return 1;
}

int
get(deporte **equipos)
{
  while (*equipos)
    get_equipo(*equipos),equipos++;
  
  return 1;
}

deporte
get_win(deporte **equipos)
{
  deporte winner = **equipos;
  
  while ( *equipos ) 
  {
    if( (*equipos)->victorias > winner.victorias)
     winner=**equipos;

  equipos++;
  }

  return winner;
}

/*
 * Allocate memory
 */
deporte **
alloc_mem(int LIM, int num_deporte)
{
  deporte **tmp = malloc(sizeof(void *)*(LIM+1));

  int i;
  for(i=0;i<LIM;i++)
    tmp[i] = malloc(sizeof(deporte))
      , tmp[i]->num_deporte = num_deporte 
      , tmp[i]->tipo_deporte = malloc(sizeof(futbol));
  tmp[LIM] = NULL;

  return tmp;
}

int
main()
{
  int LIM=0;

  printf("Numero de equipos futbol : ");
  scanf("%d", &LIM);
  deporte **equipos_futbol = alloc_mem(LIM, 0);


  printf("Numero de equipos basket : ");
  scanf("%d", &LIM);
  deporte **equipos_basket = alloc_mem(LIM, 1);

  get(equipos_futbol);

  get(equipos_basket);

  deporte winner = get_win(equipos_futbol);
  
  printf("El ganador de futbol es : %s con %d victorias.\n", winner.nombre
      , winner.victorias);

  return 1;
}
